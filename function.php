<!DOCTYPE html>
<html>
<body>

<?php  

// Create a User Defined Function in PHP
/*
function writeMsg() {
	echo " Hello World!! " ;
}

writeMsg();
*/

// PHP Function Arguments
/*
function familyName($fname) {
	echo "$fname Deo.<br>";
}
  
  familyName("Jani");
  familyName("Hege");
  familyName("Stale");
  familyName("Kai Jim");
  familyName("Borge");
*/
/*
  function familyName($fname, $year) {
  echo "$fname Refsnes. Born in $year <br>";
}

familyName("Hege","1975");
familyName("Stale","1978");
familyName("Kai Jim","1983");
*/

// PHP is a Loosely Typed Language.We try to send both a number and a string to the function without using strict
/*
function addNumbers(int $a, int $b) {
  return $a + $b;
}
echo addNumbers(5, "5 days"); 
*/

// To specify strict we need to set declare(strict_types=1);



























?>

</body>
</html>