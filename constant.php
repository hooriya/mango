<!DOCTYPE html>
<html>
<body>

<?php 

//Create a constant with a case-sensitive name:

/*
define("GREETING","Welcome to My World!!");
echo GREETING ;
*/

// Create a constant with a case-insensitive name:
/*
define("GREETING", "Welcome to W3Schools.com!", true);
echo greeting;
*/

// Create an Array constant:
/*
define("cars",["Alfa Romeo","BMW","Toyota"]);
echo cars[1]; 
*/

//This example uses a constant inside a function, even if it is defined outside the function:

define("GREETING","Welcome to My World!!");
function myTest() {
	echo GREETING;
}
myTest();





?>
</body>
</html>