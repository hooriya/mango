<!DOCTYPE html>
<html>
<body>

<?php 

// The left operand gets set to the value of the expression on the right	
/*
$x = 10 ;
echo $x;
*/

// Addition	
/* 
$x = 20 ;
$x += 100 ;
echo $x ;
*/

// Subtraction	
/*
$x = 20 ;
$x -= 100 ;
echo $x ;
*/

// Multiplication	
/*
$x = 20 ;
$x *= 100 ;
echo $x ;
*/

// Division	
/*
$x = 200 ;
$x /= 10 ;
echo $x ;
*/

// Modulus	
/*
$x = 200 ;
$x %= 3 ;
echo $x ;
*/

// Returns true if $x is equal to $y	
/*
$x = 100 ;
$y = "100";
var_dump($x==$y);
*/

// Returns true if $x is equal to $y, and they are of the same type	
/*
$x = 100 ;
$y = "100" ;
var_dump($x === $y);
*/

// Returns true if $x is not equal to $y	
/*
$x = 100 ;
$y = "100" ;
var_dump($x != $y);
*/

// Returns true if $x is not equal to $y	
/*
$x = 100 ;
$y = "100" ;
var_dump($x <> $y);
*/

// Returns true if $x is not equal to $y, or they are not of the same type	
/*
$x = 100 ;
$y = "100" ;
var_dump($x !== $y);
*/

// Returns true if $x is greater than $y	
/*
$x = 100 ;
$y = 50 ;
var_dump($x > $y);
*/

// Returns true if $x is less than $y	
/*
$x = 10;
$y = 50;

var_dump($x < $y); // returns true because $x is less than $y
*/

// Returns true if $x is greater than or equal to $y	
/*
$x = 50;
$y = 50;

var_dump($x >= $y); // returns true because $x is greater than or equal to $y
*/

// Returns true if $x is less than or equal to $y	
/*
$x = 50;
$y = 50;

var_dump($x <= $y); // returns true because $x is less than or equal to $y
 */

// Returns an integer less than, equal to, or greater than zero, depending on if $x is less than, equal to, or greater than $y. Introduced in PHP 7.	
/*
$x = 5;  
$y = 10;

echo ($x <=> $y); // returns -1 because $x is less than $y
echo "<br>";

$x = 10;  
$y = 10;

echo ($x <=> $y); // returns 0 because values are equal
echo "<br>";

$x = 15;  
$y = 10;

echo ($x <=> $y); // returns +1 because $x is greater than $y
*/

// Increments $x by one, then returns $x	
/*
$x = 10 ;
echo ++$x ;
*/

// Returns $x, then increments $x by one	
/*
$x = 10 ;
echo $x++ ;
*/

// Decrements $x by one, then returns $x	
/*
$x = 10 ;
echo --$x ;
*/

// Returns $x, then decrements $x by one	
/*
$x = 10 ;
echo $x-- ;
*/

// True if both $x and $y are true	
/*
$x = 100 ;
$y = 50 ;
if($x == 100 and $y == 50) {
	echo " Hello World !! " ;
}
*/

// True if either $x or $y is true	
/*
$x = 100;  
$y = 50;

if ($x == 100 or $y == 80) {
    echo "Hello world!";
*/

/*// True if either $x or $y is true, but not both	
$x = 100;  
$y = 50;

if ($x == 100 xor $y == 80) {
    echo "Hello world!";
}
*/

// True if both $x and $y are true	
/*
$x = 100;  
$y = 50;

if ($x == 100 && $y == 50) {
    echo "Hello world!";
}
*/

// True if either $x or $y is true
/*	
$x = 100;  
$y = 50;

if ($x == 100 || $y == 80) {
    echo "Hello world!";
}
*/

// True if $x is not true
/*
$x = 100;  

if ($x !== 90) {
    echo "Hello world!";
}
*/

// Concatenation of $txt1 and $txt2	
/*
$txt1 = " Hello " ;
$txt2 = " World! ";
echo $txt1 . $txt2 ;
*/

// Appends $txt2 to $txt1	
/*
$txt1 = "Hello";
$txt2 = " world!";
$txt1 .= $txt2;
echo $txt1;
*/

// Union of $x and $y	
/*
$x = array("a" => "Red","b" => "Green");
$y = array("c" => "Blue","d" => "Yellow");
print_r($x + $y);
*/

// Returns true if $x and $y have the same key/value pairs	
/*
$x = array("a" => "Red","b" => "Green");
$y = array("c" => "Blue","d" => "Yellow");
var_dump($x == $y);
*/

// Returns true if $x and $y have the same key/value pairs in the same order and of the same types	
/*
$x = array("a" => "Red","b" => "Green");
$y = array("c" => "Blue","d" => "Yellow");
var_dump($x === $y);
*/

// Returns true if $x is not equal to $y	
/*
$x = array("a" => "Red","b" => "Green");
$y = array("c" => "Blue","d" => "Yellow");
var_dump($x != $y);
*/

// Returns true if $x is not equal to $y	
/*
$x = array("a" => "Red","b" => "Green");
$y = array("c" => "Blue","d" => "Yellow");
var_dump($x <> $y);
*/

// Returns true if $x is not identical to $y	
/*
$x = array("a" => "Red","b" => "Green");
$y = array("c" => "Blue","d" => "Yellow");
var_dump($x !== $y);
*/

// Returns the value of $x.The value of $x is expr2 if expr1 = TRUE.The value of $x is expr3 if expr1 = FALSE   
/*
echo $status = (empty($user)) ? " anonymous " : " Logged in " ;
echo ("<br>") ;
$user = "John Doe";
echo $status = (empty($user)) ? " anonymous " : " Logged in " ;
*/

// Returns the value of $x.The value of $x is expr1 if expr1 exists, and is not NULL.If expr1 does not exist, or is NULL, the value of $x is expr2.

echo $user = $_GET["user"] ?? "anonymous";
echo("<br>");
echo $color = $color ?? "Red" ; 















?>
</body>
</html>