<!DOCTYPE html>
<html>
<body>

<?php
class myCar {
  public $color;
  public $model;
  public function __construct($color, $model) {
    $this->color = $color;
    $this->model = $model;
  }
  public function message() {
    return "My car is a " . $this->color . " " . $this->model . "!";
  }
}

$myCar = new myCar("black", "Volvo");
echo $myCar -> message();
echo "<br>";
$myCar = new myCar("red", "Toyota");
echo $myCar -> message();

?>

</body>
</html>



