<!DOCTYPE html>
<html>
<body>
	<div class="container">
	<div class="col-lg-12">
		<br><br>
		<h2 class="text-warning text-center"> Display Table Data </h2><br>
		<table class="table table-striped table-hover table-border">
			<tr class="bg-dark text-white">
				<th> Id </th>
				<th> Name </th>
				<th> Amount </th>
				<th> Acc.Number </th>
				<th> Delete </th>
				<th> Update </th>
			</tr>
			<?php 
			  include 'conn.php';

	           $q = " select * from salary ";
	           $query = mysqli_query($conn,$q);

	           while($res = mysqli_fetch_array($query)) {
?>
			 
			 <tr>
			 <td> <?php echo $res['id']; ?> </td>
			 <td> <?php echo $res['name']; ?> </td>
			 <td> <?php echo $res['amount']; ?> </td>
			 <td> <?php echo $res['accnumber']; ?> </td>
			 <td><button class="btn-danger btn" ><a href="delete.php?id=<?php echo $res['id']; ?>" class="text-white"> Delete </a></button></td>
			 <td><button class="btn-primary btn"><a href="update.php?id=<?php echo $res['id']; ?>" class="text-white"> Update </a></button></td>
			 </tr>
			 <?php 
			 }
			 ?> 	
			
		</table>
		
	</div>
		
	</div>

</body>
</html>