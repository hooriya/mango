<!DOCTYPE html>
<html>
<body>

<?php 

// The if statement executes some code if one condition is true.
/*
$t = date("H");
if ($t < "20") {
	echo "Have a Good Day!!";
}
*/

// PHP - The if...else Statement
/*
$t = date("H");
if ($t < "20") {
	echo "Have a Good Day!!";
} else {
	echo "HAve a Good Night!!";
}
*/

// PHP - The if...elseif...else Statement
/*
$t = date("H");
if ($t < "10") {
	echo "Have  a Good Morning!!";
} elseif ($t < "20") {
	echo "Have a Good Day!!";
	} else {
		echo "Have a Good Night!!";
} 
*/

// PHP - The switch Statement

$favcolor = "Purple" ;
switch ($favcolor) {
	case "Red":
	echo "Your Favorite Color is Red!";
	break;
	case "Blue":
	echo "Your Favorite Color is Blue!";
	break;
	case "Green":
	echo "Your Favorite Color is Green!";
	break;
	default:
	echo "Your Favorite Color is Neither Red , Blue or Green!";
}






























?>
</body>
</html>