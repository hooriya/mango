<!DOCTYPE html>

<?php
/*
$cookie_name = "user" ;
$cookie_value = "John Doe" ;
// setcookie($cookie_name,$cookie_value, time()+(86400 * 30),"/"); 
setcookies("test_cookie","test",time() + 3600,'/');
?>

<html>
<body>

<?php
/*
if(!isset($_COOKIE[$cookie_name]))
{
	echo " Cookie named ' " . $cookie_name . " ' is not set ! ";
} else {
	echo " Cookie ' " . $cookie_name . " ' is not set ! " ;
	echo " Value is : " . $_COOKIE[$cookie_name];
}

setcookie("user","",time() - 3600);
?>	
<?php
echo " Cookie 'user' is deleted. ";
?>
*/
setcookie("test_cookie", "test", time() + 3600, '/');
?>
<html>
<body>

<?php
if(count($_COOKIE) > 0) {
  echo "Cookies are enabled.";
} else {
  echo "Cookies are disabled.";
}
// <p><strong> Note : </strong> You Might Have to Reload the Page to See the Value of the Cookie. </p>


?>
</body>
</html>