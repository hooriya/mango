<!DOCTYPE html>
<html>
<body>

<?php 
interface Animal {
	public function makeSound();
}
class Cat implements Animal {
  public function makeSound() {
    echo " Meow !!! "."<br>" ;
   }	
}

// $animal = new Cat() ;
// $animal->makeSound() ;

class Dog implements Animal {
	public function makeSound() {
		echo " Bark !!! "."<br>" ;
	}
}

class Mouse implements Animal {
	public function makeSound() {
		echo " Squeak !!! " ;
	}
}

$cat = new Cat() ;
$dog = new Dog() ;
$mouse = new Mouse() ;
$animals = array($cat,$dog,$mouse);
foreach ($animals as $animal) {
	$animal->makeSound();
}

?>

</body>
</html>