<?php

// Use classes from the Html namespace:

/*
include "namespaces.php";

$table = new Html\Table();
$table->title = "My table";
$table->numRows = 5 ;
$row = new Html\Row();
$row->numCells = 3;
?>
*/

// Use classes from the Html namespace without the need for the Html\qualifier:

/*
namespace Html;
include "namespaces.php";

$table = new Table();
$table->title = "My table";
$table->numRows = 5 ;
$row = new Row();
$row->numCells = 4;
*/

// Give a namespace an alias:
/*
include "namespaces.php";
use Html as H ;
$table = new H\Table();
$table->title = "My table";
$table->numRows = 5 ;
*/

// Give a class an alias:

include "namespaces.php";
use Html\Table as T ;
$table = new T();
$table->title = "My table";
$table->numRows = 6 ;

?>

<html>
<body>

	<?php $table->message(); ?>
	<?php // $row->message(); ?>

</body>
</html>