<!DOCTYPE html>
<html>
<body>

<?php


/*
$txt = "Hello world!";
$x = 5;
$y = 10.5;

echo $txt;
echo "<br>";
echo $x;
echo "<br>";
echo $y;



// The PHP echo statement is often used to output data to the screen.
/*
echo " I Love $txt! " ;
echo " I Love " . $txt . "!!" ;
*/



/*
$x = 5 ;
$y = 4 ;
echo $x + $y ;
*/



// outside a function has a GLOBAL SCOPE
/*
$x = 5 ;
function myTest() {
	echo " <p> Variable x inside function is : $x </p> ";
}
myTest();
echo " <p> Variable x outside function is : $x </p> " ;
*/


// within a function has a GLOBAL SCOPE
/*
function myTest() {
	$x = 5 ;
	echo " <p> Variable x inside function is : $x </p> "; 
}
myTest();
	echo " <p> Variable x outside function is : $x </p> "; 
*/


// The global keyword is used to access a global variable from within a function.
/*
$x = 5 ;
$y = 10 ;

function myTest() {
	global $x , $y ;
	$y = $x + $y ;
}
myTest();
echo $y ;
*/

// an array called $GLOBALS[index]
/*
$x = 5 ;
$y = 15 ;
function myTest() {
	$GLOBALS['y'] = $GLOBALS['x'] + $GLOBALS['y'];
}
myTest();
echo $y;
*/ 

// use the static keyword when you first declare the variable

function myTest() {
	static $x = 0 ;
	echo $x ;
	$x++ ;
}

myTest();
echo "<br>" ;
myTest();
echo "<br>" ;
myTest();
echo "<br>" ;
myTest();




?>
</body>
</html>
