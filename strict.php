<?php // declare(strict_types=1);

/*
function addNumbers(int $a,int $b) {
	return $a + $b ;
}
echo addNumbers(5,"5 days");
*/

//PHP Default Argument Value
/*
function setHeight(int $minheight = 50) {
	echo "The Height is : $minheight <br>";
}

setHeight(350);
setHeight();
setHeight(135);
setHeight(80);
*/

// PHP Functions - Returning values
/*
function sum(int $x,int $y) {
	$z =$x + $y ;
	return $z ;
}

echo "5 + 10 = " . sum(5,10) . "<br>" ;
echo "7 + 13 = " . sum(7,13) . "<br>" ;
echo "5 + 4 = " . sum(5,4) . "<br>" ;
echo "4 + 3 = " . sum(4,3) . "<br>" ;
*/

// PHP Return Type Declarations
/*
function addNumbers(float $a ,float $b) : float {
	return $a + $b ;
}
echo addNumbers(1.2,5.2);
*/

/*
function addNumbers(float $a ,float $b) : int {
	return (int)($a + $b) ;
}
echo addNumbers(1.2,5.2);
*/

// Use a pass-by-reference argument to update a variable:

function add_five(&$value) {
	$value += 5 ;
}

$num = 2 ;
add_five($num);
echo $num ;








?>

















