<!DOCTYPE html>
<html>
<body>

<?php

// try to throw an exception without catching it
/*
function divide($dividend,$divisor){
	if($divisor == 0){
		throw new Exception("Division By Zero");
	}
	return $dividend / $divisor ;
}

echo divide (5,0);
*/

// Show a message when an exception is thrown
/*
function divide($dividend,$divisor){
	if($divisor == 0){
		throw new Exception("Division By Zero");
	}
	return $dividend / $divisor ;
}
try {
echo divide (5,0);
} catch(Exception $e){
	echo "Unable to Divide.";
}
*/

// Show a message when an exception is thrown and then indicate that the process has ended
/*
function divide($dividend,$divisor){
	if($divisor == 0){
		throw new Exception("Division By Zero");
	}
	return $dividend / $divisor ;
}
try {
echo divide (5,0);
} catch(Exception $e){
	echo "Unable to Divide.";
} finally {
	echo " Process Complete. ";
}
*/

// Output a string even if an exception was not caught:
/*
function divide($dividend, $divisor) {
  if($divisor == 0) {
    throw new Exception("Division by zero");
  }
  return $dividend / $divisor;
}

try {
  echo divide(5, 0);
} finally {
  echo 'Process complete.';
}
*/

 // Output information about an exception that was thrown:

function divide($dividend, $divisor) {
  if($divisor == 0) {
    throw new Exception("Division by zero", 1);
  }
  return $dividend / $divisor;
}

try {
  echo divide(5, 0);
} catch(Exception $ex) {
  $code = $ex->getCode();
  $message = $ex->getMessage();
  $file = $ex->getFile();
  $line = $ex->getLine();
  echo "Exception thrown in $file on line $line: [Code $code]
  $message";
}
?>
	
</body>
</html>