<?php
session_start();
?>

<!DOCTYPE html>
<html>
<body>

<?php
/*
$_SESSION["favcolor"]="Green";
$_SESSION["favanimal"]="Cat";

// Start a PHP Session

echo " Session Variables are Set.";

// Get PHP Session Variable Values
 
echo " Favorite Color is " . $_SESSION["favcolor"] . "<br>";
echo " Favorite Animal is " . $_SESSION["favanimal"] . ".";

// Another way to show all the session variable values for a user session

print_r($_SESSION);

// Modify a PHP Session Variable

$_SESSION["favcolor"]="Yellow";
print_r($_SESSION);
*/

// Destroy a PHP Session
// remove all session variables
session_unset();

// destroy the session
session_destroy();

echo "All session variables are now removed, and the session is destroyed."

?>


</body>
</html>