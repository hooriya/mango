<<!DOCTYPE html>
<html>
<body>

<?php


// Return the Length of a String
// echo strlen("Hello World!!");

// str_word_count() - Count Words in a String
// echo str_word_count("Hello World!!");

// strrev() - Reverse a String
// echo strrev("Hello World!!");

// strpos() - Search For a Text Within a String
// echo strpos("Hello World!!", "World");

// str_replace() - Replace Text Within a String
echo str_replace("World","Dolly", "Hello World!!");

?>
</body>
</html>