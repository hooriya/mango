<!DOCTYPE html>
<html>
<body>

<?php 

// The while loop executes a block of code as long as the specified condition is true.
/*
$x = 1;
while($x <= 5) {
	echo "The Number is: $x <br>";
	$x++ ;
}
*/

// The while loop executes a block of code as long as the specified condition is true.
/*
$x = 0;
while($x <= 100) {
	echo "The Number is: $x <br>";
	$x+=10 ;
}
*/

// The PHP do...while Loop
/*
$x = 1 ;
do {
	echo " The umber is : $x <br>";
	$x++ ;
} while ($x <= 5); 
*/

// The PHP for Loop
/*
for ($x = 0 ; $x <= 10 ; $x++) {
	echo " The Number is : $x <br> " ;
}
*/

// The PHP foreach Loop
/*
$colors = array("Red","Blue","Green","Yellow");
foreach ($colors as $value){
echo "$value <br>";
}
*/

// output both the keys and the values of the given array ($age)
/*
$age = array("Peter" => "35","Ben" => "42","Joe" => "47");
foreach ($age as $x => $val) {
	echo "$x = $val <br>" ;
}
*/

// PHP Break
/*
for ($x = 0 ; $x <= 10 ; $x++) {
	if ($x == 6) {
		break ;
	}
	echo " The Number is : $x <br> ";
}
*/

// PHP Continue
/*
for ($x = 0 ; $x < 10 ; $x++) {
	if ($x == 5)  { 
	continue ;
}
echo " The Number is : $x <br> ";
}
*/

// Break in While Loop
/*
$x = 0 ;
while ($x < 10) {
	if ($x == 4) {
		break ;
	}
	echo " The Number is : $x <br> " ;
	$x++ ;
}
*/

// Continue in While Loop

$x = 0 ;
while ($x < 10) {
	if ($x == 4) {
		$x++ ;
		continue ;
	}
	echo " The Number is : $x <br> " ;
	$x++ ;
}









?>
</body>
</html>