<!DOCTYPE html>
<html>
<body>

<?php
/*
class fruit {
	public $name ;
	public $color ;
	function set_name($name)
	{
		$this->name = $name ;
	}
	function get_name() 
	{
        return $this->name ;
	}	
	function set_color($color){
		$this -> color = $color ;
	}
	function get_color(){
		return $this -> color ;
	}
}

$apple = new Fruit();
// $banana = new Fruit();
$apple -> set_name('Apple');
// $banana -> set_name('Banana');
$apple -> set_color('Red');
// echo $apple -> get_name();
// echo "<br>";
// echo $banana -> get_name();
echo " Name : " . $apple -> get_name();
echo "<br>" ;
echo " Color : " .$apple -> get_color();
*/

// 1. Inside the class (by adding a set_name() method and use $this)
/*
class Fruit {
  public $name;
  function set_name($name) {
    $this->name = $name;
  }
}
$apple = new Fruit();
$apple->set_name("Apple");

echo $apple->name;
*/

// 2. Outside the class (by directly changing the property value)
/*
class Fruit {
  public $name;
}
$apple = new Fruit();
$apple->name = "Apple";

echo $apple->name;
*/

// PHP - instanceof

class Fruit {
  
  public $name;
  public $color;

    function set_name($name) {
    $this->name = $name;
  }
  function get_name() {
    return $this->name;
  }
}

$apple = new Fruit();
var_dump($apple instanceof Fruit);


?>

</body>
</html>