<!DOCTYPE html>
<html>
<body>

<?php 

// readfile() function 
/* 
echo readfile("webdictionary.txt");
*/

// fopen() function
/*
$myfile = fopen("webdictionary.txt", "r") or die ("Unable to open file!");
echo fread($myfile,filesize("webdictionary.txt"));
fclose($myfile);
*/

// fclose() function
/*
<?php
$myfile = fopen("webdictionary.txt", "r");
fclose($myfile);
?>
*/

// Read Single Line - fgets()
/*
<?php
$myfile = fopen("webdictionary.txt", "r") or die("Unable to open file !");
echo fgets($myfile);
fclose($myfile);
?>
*/

// Check End-Of-File - feof()
/*
$myfile = fopen("webdictionary.txt", "r") or die ("Unable to open file !");
while (!feof($myfile)) {
	echo fgets($myfile) . "<br>";
}
fclose($myfile);
*/

// Read Single Character - fgetc()

$myfile = fopen("webdictionary.txt","r") or die ("Unable to open file !");
while (!feof($myfile)) {
	echo fgetc($myfile);
}
fclose($myfile);



?>
</body>
</html>