<!DOCTYPE html>
<html>
<body>

<?php

// We can access a constant from outside the class 
/*
class GoodBye {
	const LEAVING_MESSAGE = " Thank You For Visiting W3Schools.com!";
}
echo GoodBye :: LEAVING_MESSAGE ;
*/

// We can access a constant from inside the class 

class GoodBye {
	const LEAVING_MESSAGE = " Thank You For Visiting !!" ;
	public function byebye() {
		echo self::LEAVING_MESSAGE ;
	}
}
$goodbye = new GoodBye() ;
$goodbye->byebye() ;





?>
</body>
</html>