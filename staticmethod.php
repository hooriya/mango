<!DOCTYPE html>
<html>
<body>

<?php

// Static Method 
/*
class greeting {
  public static function welcome() {
    echo " Hello World! " ;
 } 
}
greeting::welcome();
*/

// More on Static Methods
/*
 class greeting {
 	public static function welcome() {
 		echo " Hello World !! ";
 	}

 	public function __construct() {
 		self::welcome() ;
 	}
 }

new greeting();
*/

//  static method should be public
/*
class greeting {
	public static function welcome() {
		echo " Hello World !! " ;
	}
}
class SomeOtherClass {
	public function message() {
		greeting::welcome() ;
	}
}
*/

// the static method can be public or protected

class domain {
	protected static function getWebsiteName() {
		return "W3Schools.com" ;
	}
}

class domainW3 extends domain {
	public $getWebsiteName ;
	public function __construct() {
		$this -> getWebsiteName = parent::getWebsiteName();
	}
}

$domainW3 = new domainW3;
echo $domainW3 -> getWebsiteName;

?>

</body>
</html>