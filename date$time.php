<!DOCTYPE html>
<html>
<body>

 <?php

 // Get a Date
/*
echo " Today is " . date("Y-m-d") . "<br>";
echo " Today is " . date("Y/m/d") . "<br>";
echo " Today is " . date("Y.m.d") . "<br>";
echo " Today is " . date("l") ;
*/

/*
// Automatic Copyright Year
&copy 2010-<?php echo date("Y");?>
*/

// Get a Time
// echo " The Time is " . date("h:i:s a");
 
// Get Your Time Zone
/* 
date_default_timezone_set("America/New_York");
echo " The Time is " . date("h:i:s a");
*/

// Create a Date With mktime()
/* 
$d = mktime(11,14,54,8,12,2014);
echo " Created Date is " . date("Y-m-d h:i:s a",$d);
*/

// Create a Date From a String With strtotime()
/* 
$d = strtotime("10:30 PM April 15 2018");
echo " Created Date is ". date("Y-m-d h:i:s a", $d);
*/

// Converting a string to a date
/*
$d = strtotime ("tomorrow");
echo date("Y-m-d h:i:s a",$d) . "<br>";
$d = strtotime ("next Saturday");
echo date("Y-m-d h:i:s a",$d) . "<br>";
$d = strtotime (" +3 Months ");
echo date("Y-m-d h:i:s a",$d) . "<br>";
*/

// Outputs the dates for the next six Saturdays
/*
$startdate = strtotime ("Saturday");
$enddate = strtotime(" +6 weeks ",$startdate);
while ($startdate < $enddate) {
	echo date ("M d",$startdate) . "<br>";
	$startdate = strtotime (" +1 week " ,$startdate);
}
*/

// Outputs the number of days until 4th of July

$d1 = strtotime (" July 04 ");
$d2 = ceil(($d1- time())/60/60/24);
echo " There are " . $d2 . " days until 4th of July. ";




?>

</body>
</html>