<!DOCTYPE html>
<html>
<body>

<?php
/*
class Fruit {
	public $name ;
	public $color ;
	public function __construct($name,$color) {
		$this->name = $name ;
		$this->color = $color ;
	}
	public function intro() {
		echo " The Fruit is {$this->name} and the Color is {$this->color}." ;
	}
}
class Strawberry extends Fruit {
	public function message() {
		echo " Am I a Fruit or Berry ? " ;
	}
}
$strawberry = new Strawberry ("Strawberry","Red");
$strawberry->message();
$strawberry->intro();
*/

// PHP - Inheritance and the Protected Access Modifier
/*
class Fruit {
  public $name;
  public $color;
  public function __construct($name, $color) {
    $this->name = $name;
    $this->color = $color; 
  }
  protected function intro() {
    echo "The fruit is {$this->name} and the color is {$this->color}."; 
  }
}

class Strawberry extends Fruit {
  public function message() {
    echo "Am I a fruit or a berry? "; 
 
  }
}

// Try to call all three methods from outside class
$strawberry = new Strawberry("Strawberry", "red");  // OK. __construct() is public
$strawberry->message(); // OK. message() is public
$strawberry->intro(); // ERROR. intro() is protected
*/

// call the protected method (intro()) from inside the derived class.
/*
class Fruit {
  public $name;
  public $color;
  public function __construct($name, $color) {
    $this->name = $name;
    $this->color = $color; 
  }
  protected function intro() {
    echo "The fruit is {$this->name} and the color is {$this->color}."; 
  }
}

class Strawberry extends Fruit {
  public function message() {
    echo "Am I a fruit or a berry? "; 
    $this-> intro();
   }
}
$strawberry = new Strawberry("Strawberry","Red"); 
$strawberry->message();
*/

// PHP - Overriding Inherited Methods
/*
class Fruit {
	public $name ;
	public $color ;
	public function __construct($name,$color) {
		$this->name = $name;
		$this->color = $color;
	}
	public function intro() {
		echo " The fruit is {$this->name} and the Color is {$this->color}." ;
	} 
}

class Strawberry extends Fruit {
	public $weight;
	public function __construct($name,$color,$weight) {
		$this->name = $name ;
		$this->color = $color ;
		$this->weight = $weight ;
	}
	public function intro() {
		echo " The fruit is {$this->name}, the color is {$this->color}, and the weight is {$this->weight} grm." ;
	}
}
$strawberry = new Strawberry ("Strawberry","Red","50");
$strawberry->intro();
*/

// PHP - The final Keyword
/*
final class Fruit {
	public $name ;
	public $color ;
	public function __construct($name,$color) {
		$this->name = $name ;
		$this->color = $color ;
	}
	public function intro() {
		echo " The Fruit is {$this->name} and the Color is {$this->color}." ;
	}
}
class Strawberry extends Fruit {
	public function message() {
		echo " Am I a Fruit or Berry ? " ;
	}
}
$strawberry = new Strawberry ("Strawberry","Red");
$strawberry->message();
$strawberry->intro();
*/

// PHP - The final Keyword
/*
final class Fruit {
}

class Strawberry extends Fruit {
}
*/

// how to prevent method overriding

class Fruit {
	final public function intro() {
		}
	}
	class Strawberry extends Fruit {
public function intro(){
	
}
}


?>
</body>
</html>