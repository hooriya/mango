<!DOCTYPE html>
<html>
<body>

<?php 

/* A string is a sequence of characters, like "Hello world!".
A string can be any text inside quotes. You can use single or double quotes: */
/*
$x = " Hello World! <br> ";
$y = ' Hello World! ';
echo $x ;
echo "<br>" ;
echo $y ;
*/

// An integer data type is a non-decimal number between -2,147,483,648 and 2,147,483,647
/*
$x = 5985 ;
var_dump($x);
*/

// A float (floating point number) is a number with a decimal point or a number in exponential form.
/*
$x = 10.365 ;
var_dump($x);
*/

// An array stores multiple values in one single variable.
/*
$cars = array("Volvo","BMW","Toyota");
var_dump($cars);
*/

// Classes and objects are the two main aspects of object-oriented programming.
/*
class Car {
  public $color;
  public $model;
  public function __construct($color, $model) {
    $this->color = $color;
    $this->model = $model;
  }
  public function message() {
    return "My car is a " . $this->color . " " . $this->model . "!";
  }
}

$myCar = new Car("black", "Volvo");
echo $myCar -> message();
echo "<br>";
$myCar = new Car("red", "Toyota");
echo $myCar -> message();
*/

// Null is a special data type which can have only one value: NULL.

$x = "Hello world!";
$x = null;
var_dump($x);
?>

</body>
</html>