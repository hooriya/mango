<!DOCTYPE html>
<html>
<body>

<?php
/*
trait message1 {
	public function msg1() {
		echo " OOP IS Fun !! " ;
	}
}

class Welcome {
	use message1 ;
}

$obj = new Welcome();
$obj->msg1();
*/

trait message1 {
	public function msg1() {
		echo " OOP Is Fun !! " ;
	}
}

trait message2 {
	public function msg2() {
		echo " OOP Reduces Code Duplication !! " ;
	}
}
class Welcome {
	use message1;
} 
class Welcome2 {
	use message1 , message2 ;
}

$obj = new Welcome() ;
$obj->msg1() ;
echo "<br>" ;

$obj2 = new Welcome2() ;
$obj2->msg1() ;
$obj2->msg2() ;







?>

</body>
</html>