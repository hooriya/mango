<!DOCTYPE html>
<html>
<body>

<?php
// use the class name
/*
class pi {
	public static $value = 3.14159;
}
echo pi::$value;
*/

// using the self keyword 
/*
class pi {
	public static $value = 3.14159 ;
	public function staticValue() {
		return self::$value;
	}
}
$pi = new pi();
echo $pi->staticValue();
*/

// use the parent keyword inside the child class

class pi {
	public static $value = 3.14159 ;
}
 class x extends pi {
 	public function xStatic() {
 		return parent::$value ;
 	}
 }
echo x::$value ;
$x = new x();
echo $x->xStatic();




















?>
</body>
</html>