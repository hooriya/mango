<!DOCTYPE html>
<html>
<body>

<?php

// to encode an associative array into a JSON object
/*
$age = array("Peter"=>34,"Ben"=>54,"Joe"=>37);
echo json_encode($age);
*/

// to encode an indexed array into a JSON object
/*
$cars = array("Volvo","Toyota","BMW");
echo json_encode($cars);
*/

// decodes JSON data into a PHP object
/*
$jsoneobj = '{"Peter":35,"Ben":37,"Joe":43}';
var_dump(json_decode($jsoneobj));
*/

// decodes JSON data into a PHP associative array
/*
$jsoneobj = '{"Peter":35,"Ben":37,"Joe":43}';
var_dump(json_decode($jsoneobj,true));
*/

// Accessing the Decoded Values //
// how to access the values from a PHP object
/*
$jsoneobj = '{"Peter":67,"Ben":34,"Joe":43}';
$obj = json_decode($jsoneobj);

echo $obj->Peter ."<br>" ;
echo $obj->Ben ."<br>" ;
echo $obj->Joe ."<br>" ;
*/

// how to loop through the values of a PHP associative array

$jsonobj = '{"Peter":35,"Ben":38,"Joe":43}';
$arr = json_decode($jsonobj,true);
foreach ($arr as $key => $value) {
	echo $key . " -> " . $value . "<br>";
}

?>

</body>
</html>
