<!DOCTYPE html>
<html>
<body>

<?php
// PHP include 1
/*
<h1> Welcome to My Home Page! </h1>
<p> Some Text. </p>
<p> Some More Text. </p>
 <?php include 'footer.php'; ?> 
*/

// PHP include 2
/*
<div class="menu">
<?php include 'menu.php';?>
</div>

<h1>Welcome to my home page!</h1>
<p>Some text.</p>
<p>Some more text.</p>
*/

// PHP Include 3
/*
<h1> Welcome to My Home Page </h1>
<?php include 'vars.php';
echo " I have a $color $car ";
?>
*/

// PHP include vs. require
/*
<h1>Welcome to My Home Page!</h1>
<?php require 'noFileExists.php';
echo "I have a $color $car.";
?>
*/

//  



?>
</body>
</html>








