<!DOCTYPE html>
<html>
<body>

<?php

// how to output text with the echo command
/*
echo "<h2>PHP is Fun!</h2>";
echo "Hello world!<br>";
echo "I'm about to learn PHP!<br>";
echo "This ", "string ", "was ", "made ", "with multiple parameters.";
*/

// how to output text and variables with the echo statement
/*
$txt1 = "Learn PHP";
$txt2 = "W3Schools.com";
$x = 5;
$y = 4;

echo "<h4>" . $txt1 . "</h4>";
echo " Study PHP at " . $txt2 . "<br><br>";
echo $x + $y;
*/

// how to output text with the print command 
/*
print "<h2>PHP is Fun!</h2>";
print "Hello world!<br>";
print "I'm about to learn PHP!";
*/

// how to output text and variables with the print statement

$txt1 = " Learn PHP ";
$txt2 = " PHP ";
$x = 5 ;
$y = 4 ;

print "<h4>" . $txt1 . "</h4>" ;
print " I Love " . $txt2 . "<br><br>" ;
print $x = $y ;

?> 
</body>
</html>