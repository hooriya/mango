<!DOCTYPE html>
<html>
<body>

<?php
/*
function my_callback($item)
{
	return strlen($item);
}
$strings = ["apple","banana","orange","coconut"];
// $lengths = array_map("my_callback",$strings); // Pass a callback
$lengths = array_map(function($item){ return strlen($item); }, $strings); // Use an anonymous function
print_r($lengths);
*/

// Callbacks in User Defined Functions

function exclaim($str){
	return $str . " ? ";
}
function ask($str){
	return $str . " ? ";
}

function printFormatted($str,$format){
	echo $format($str);
}
printFormatted ("Hello World","exclaim");
printFormatted ("Hello World","ask");


?>

</body>
</html>